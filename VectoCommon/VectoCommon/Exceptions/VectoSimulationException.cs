﻿/*
* This file is part of VECTO.
*
* Copyright © 2012-2019 European Union
*
* Developed by Graz University of Technology,
*              Institute of Internal Combustion Engines and Thermodynamics,
*              Institute of Technical Informatics
*
* VECTO is licensed under the EUPL, Version 1.1 or - as soon they will be approved
* by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use VECTO except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/community/eupl/og_page/eupl
*
* Unless required by applicable law or agreed to in writing, VECTO
* distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and
* limitations under the Licence.
*
* Authors:
*   Stefan Hausberger, hausberger@ivt.tugraz.at, IVT, Graz University of Technology
*   Christian Kreiner, christian.kreiner@tugraz.at, ITI, Graz University of Technology
*   Michael Krisper, michael.krisper@tugraz.at, ITI, Graz University of Technology
*   Raphael Luz, luz@ivt.tugraz.at, IVT, Graz University of Technology
*   Markus Quaritsch, markus.quaritsch@tugraz.at, IVT, Graz University of Technology
*   Martin Rexeis, rexeis@ivt.tugraz.at, IVT, Graz University of Technology
*/

using System;
using System.Runtime.Serialization;
using TUGraz.VectoCommon.Models;

namespace TUGraz.VectoCommon.Exceptions
{
	[Serializable]
	public class VectoSimulationException : VectoException
	{
		public VectoSimulationException(string msg) : base(msg) {}
		public VectoSimulationException(string msg, Exception inner) : base(msg, inner) {}

		//[StringFormatMethod("message")]
		public VectoSimulationException(string message, params object[] args) : base(message, args) {}

		//[StringFormatMethod("message")]
		public VectoSimulationException(string message, Exception inner, params object[] args) : base(message, inner, args) {}

		protected VectoSimulationException(SerializationInfo info, StreamingContext context) : base(info, context) { }
	}

	[Serializable]
	public class VectoEngineSpeedTooLowException : VectoSimulationException
	{
		public VectoEngineSpeedTooLowException(string msg) : base(msg) {}
		public VectoEngineSpeedTooLowException(string msg, Exception inner) : base(msg, inner) {}
		public VectoEngineSpeedTooLowException(string message, params object[] args) : base(message, args) {}

		public VectoEngineSpeedTooLowException(string message, Exception inner, params object[] args)
			: base(message, inner, args) {}
	}

	[Serializable]
	public class UnexpectedResponseException : VectoSimulationException
	{
		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
			info.AddValue("Response", Response);
		}

		protected UnexpectedResponseException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			Response = (IResponse)info.GetValue("Response", typeof(IResponse));
		}

		public readonly IResponse Response;

		public UnexpectedResponseException(string message, IResponse resp)
			: base(message + Environment.NewLine + "{0}", resp)
		{
			Response = resp;
		}
	}

	[Serializable]
	public class VectoSearchFailedException : VectoException
	{
		public VectoSearchFailedException(string message, params object[] args) : base(message, args) {}
	}

	[Serializable]
	public class VectoSearchAbortedException : VectoException
	{
		public VectoSearchAbortedException(string message, params object[] args) : base(message, args) { }
	}
}