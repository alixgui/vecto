﻿using System.Windows.Controls;

namespace VECTO3GUI2020.Views.JobEditViews.Vehicle.Components
{
    /// <summary>
    /// Interaction logic for AxleWheelsView_v2_0.xaml
    /// </summary>
    public partial class AxleWheelsView_v2_0 : UserControl
    {
        public AxleWheelsView_v2_0()
        {
            InitializeComponent();
        }
    }
}
