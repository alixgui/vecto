## Supported Powertrain Architectures

The following xEV architectures are currently supported in VECTO. All architectures can be used together with the bus auxiliaries model in engineering mode.

### Parallel Hybrid Electric Vehicle Architectures

* P1 + AMT
* P1 + APT-S/P
* P2 + AMT
* P2.5 + AMT
* P2.5 + APT-S/P
* P3 + AMT
* P3 + APT-S/P
* P4 + AMT
* P4 + APT-S/P

### Pure Electric Vehicle Architectures

* E2 + AMT
* E2 + APT-N
* E3
* E4
