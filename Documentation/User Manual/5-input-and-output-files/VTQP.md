## Vehicle Boosting Limits (.vtqp)

This file contains the vehicle's boosting limits depending on the combustion engine's angular speed. The file uses the [VECTO CSV format](#csv).

- Filetype: .vtqp
- Header: **n [rpm] , T_drive [Nm]**
- Requires at least 2 data entries

**Example:**

~~~
n [rpm]             , T_drive [Nm]
0                   , 1200
600                 , 0
3000                , 0
~~~
