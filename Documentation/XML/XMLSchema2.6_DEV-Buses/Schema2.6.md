XML Schema 2.6 DEV
===============

  * New Vehicle type: Heavy Bus Primary Vehicle
  * New Vehiclee type: Heavy Bus Completed Vehicle


##PrimaryVehicleDeclarationType

*Namespace:* urn:tugraz:ivt:VectoAPI:DeclarationDefinitions:DEV:v2.6

*Base Type:* v2.0:AbstractVehicleDeclarationType

![](XMLSchema2.6_DEV-Buses/PrimaryVehicleDeclarationType.png)

##PrimaryVehicleComponentsDeclarationType

*Namespace:* urn:tugraz:ivt:VectoAPI:DeclarationDefinitions:DEV:v2.6

*Base Type:* v2.6:AbstractPrimaryVehicleComponentsDeclarationType

![](XMLSchema2.6_DEV-Buses/PrimaryVehicleComponentsDeclarationType.png)

##PrimaryVehicleAuxiliaryDataDeclarationType

*Namespace:* urn:tugraz:ivt:VectoAPI:DeclarationDefinitions:DEV:v2.6

*Base Type:* v2.6:AbstractPrimaryVehicleAuxiliaryDataDeclarationType

![](XMLSchema2.6_DEV-Buses/PrimaryVehicleAuxiliairesDataDeclarationType.png)


##CompletedVehicleDeclarationType

*Namespace:* urn:tugraz:ivt:VectoAPI:DeclarationDefinitions:DEV:v2.6

*Base Type:* v2.0:AbstractVehicleDeclarationType

![](XMLSchema2.6_DEV-Buses/CompletedVehicleDeclarationType.png)

##CompletedVehicleComponentsDeclarationType

*Namespace:* urn:tugraz:ivt:VectoAPI:DeclarationDefinitions:DEV:v2.6

*Base Type:* v2.AbstractCompletedVehicleComponentsDeclarationType

![](XMLSchema2.6_DEV-Buses/CompletedVehicleComponentsDeclarationType.png)

##CompletedVehicleAuxiliaryDataDeclarationType

*Namespace:* urn:tugraz:ivt:VectoAPI:DeclarationDefinitions:DEV:v2.6

*Base Type:* v2.6:AbstractCompletedVehicleAuxiliaryDataDeclarationType

![](XMLSchema2.6_DEV-Buses/CompletedVehicleAuxiliariesDataDeclarationType.png)

##VehicleMediumLorryDeclarationType

*Namespace:* urn:tugraz:ivt:VectoAPI:DeclarationDefinitions:DEV:v2.6

*Base Type:* v2.0:AbstractVehicleDeclarationType

![](XMLSchema2.6_DEV-Buses/VehicleMediumLorryDeclarationType.png)

##VehicleComponentsNoAxlegearType

*Namespace:* urn:tugraz:ivt:VectoAPI:DeclarationDefinitions:DEV:v2.6

*Base Type:* v2.0:AbstractVehicleComponentsDeclarationType

![](XMLSchema2.6_DEV-Buses/VehicleComponentsNoAxlegearType.png)

##GearboxDataDeclarationType

*Namespace:* urn:tugraz:ivt:VectoAPI:DeclarationDefinitions:DEV:v2.6

*Base Type:* v2.0:AbstractTransmissionDataDeclarationType

![](XMLSchema2.6_DEV-Buses/GearboxDataDeclarationType.png)
