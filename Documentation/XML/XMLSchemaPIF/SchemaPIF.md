XML Schema Primary Vehicle Information File
===========================================

VECTO Output generated for primary vehicles (heavy bus). Used as input for the second stage simulation.

##VectoOutputPrimaryVehicle
![](XMLSchemaPIF/VectoOutputPrimaryVehicle.png)

##PrimaryVehicleHeavyBusDataType
![](XMLSchemaPIF/PrimaryVehicleHeavyBusDataType.png)

##VehiclePIFType
![](XMLSchemaPIF/VehiclePIFType.png)

##VehicleComponentsPIFType
![](XMLSchemaPIF/VehicleComponentsPIFType.png)

##EngineDataPIFType
![](XMLSchemaPIF/EngineDataPIFType.png)

##TransmissionDataPIFType
![](XMLSchemaPIF/TransmissionDataPIFType.png)

##TransmissionGearsPIFType
![](XMLSchemaPIF/TransmissionGearsPIFType.png)

##AngledriveDataPIFType
![](XMLSchemaPIF/AngledriveDataPIFType.png)

##AxlegearDataPIFType
![](XMLSchemaPIF/AxlegearDataPIFType.png)





