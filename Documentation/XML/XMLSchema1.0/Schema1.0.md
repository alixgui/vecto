XML Schema 1.0
===============

XML schema for declaration input data according to EU Regulation 2017/2400 for component data and job data.
*Note: has been updated a few times to take into account changes in the regulation*

##VectoInputDeclaration

*Namespace:* urn:tugraz:ivt:VectoAPI:DeclarationInput:v1.0

![](XMLSchema1.0/VectoInputDeclaration.png)

##VectoInputComponent

*Namespace:* urn:tugraz:ivt:VectoAPI:DeclarationComponent:v1.0

![](XMLSchema1.0/VectoInputComponent.png)

##VehicleDeclarationType

*Namespace:* urn:tugraz:ivt:VectoAPI:DeclarationDefinitions:v1.0

![](XMLSchema1.0/VehicleDeclarationType.png)

##VehicleComponentsType

*Namespace:* urn:tugraz:ivt:VectoAPI:DeclarationDefinitions:v1.0

![](XMLSchema1.0/VehicleComponentsType.png)

##EngineDataDeclarationType

*Base Type:* v1.0:AbstractCombustionEngineDataDeclarationType*

![](XMLSchema1.0/EngineDataDeclarationType.png)

##GearboxDataDeclarationType

*Namespace:* urn:tugraz:ivt:VectoAPI:DeclarationDefinitions:v1.0

*Base Type:* v1.0:AbstractTransmissionDataDeclarationType

![](XMLSchema1.0/GearboxDataDeclarationType.png)

##GearDataDeclarationType

*Namespace:* urn:tugraz:ivt:VectoAPI:DeclarationDefinitions:v1.0

*Base Type:* v1.0:GearBaseType

![](XMLSchema1.0/GearDeclarationType.png)

##TorqueConverterDataDeclarationType

*Namespace:* urn:tugraz:ivt:VectoAPI:DeclarationDefinitions:v1.0

*Base Type:* v1.0:AbstractTorqueConverterDataDeclarationType

![](XMLSchema1.0/TorqueConverterDataDeclarationType.png)

##AngledriveDataDeclarationType

*Namespace:* urn:tugraz:ivt:VectoAPI:DeclarationDefinitions:v1.0

*Base Type:* v1.0:AbstractAngledriveDataDeclarationType*

![](XMLSchema1.0/AngledriveDataDeclarationType.png)

##RetarderDataDeclarationType

*Namespace:* urn:tugraz:ivt:VectoAPI:DeclarationDefinitions:v1.0

*Base Type:* v1.0:AbstractRetarderDataDeclarationType

![](XMLSchema1.0/RetarderDataDeclarationType.png)

##AxlegearDataDeclarationType

*Namespace:* urn:tugraz:ivt:VectoAPI:DeclarationDefinitions:v1.0

*Base Type:* v1.0:AbstractAxlegearDataDeclarationType

![](XMLSchema1.0/AxlegearDataDeclarationType.png)

##AxleWheelsDataDeclarationType

*Namespace:* urn:tugraz:ivt:VectoAPI:DeclarationDefinitions:v1.0

![](XMLSchema1.0/AxleWheelsDataDeclarationType.png)

##AxleDeclarationType

*Namespace:* urn:tugraz:ivt:VectoAPI:DeclarationDefinitions:v1.0

![](XMLSchema1.0/AxleDeclarationType.png)

##TyreDataDeclarationType

*Namespace:* urn:tugraz:ivt:VectoAPI:DeclarationDefinitions:v1.0

*Base Type:* v1.0:AbstractTyreDataDeclarationType

![](XMLSchema1.0/TyreDataDeclarationType.png)

##AuxiliariesDataDeclarationType

*Namespace:* urn:tugraz:ivt:VectoAPI:DeclarationDefinitions:v1.0

*Base Type:* v1.0:AuxiliariesBaseType

![](XMLSchema1.0/AuxiliariesDataDeclarationType.png)

##AirdragDataDeclarationType

*Namespace:* urn:tugraz:ivt:VectoAPI:DeclarationDefinitions:v1.0

*Base Type:* v1.0:AbstractAirdragDataDeclarationType

![](XMLSchema1.0/AirDragDataDeclarationType.png)

##SignatureType

*Namespace:* urn:tugraz:ivt:VectoAPI:DeclarationDefinitions:v1.0

![](XMLSchema1.0/SignatureType.png)
